package start;

import java.sql.SQLException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.ClientBLL;
import bll.BillBLL;
import bll.ProductBLL;
import model.Client;
import model.Bill;
import model.Product;
import presentation.Controller;
import presentation.View;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException, NoSuchMethodException {
		
		View view=new View();
		Controller controller=new Controller(view);
		ClientBLL clientBll = new ClientBLL();
		BillBLL orderBll = new BillBLL();
		ProductBLL productBll = new ProductBLL();

		Client client1 = new Client(4,"name","email@yahoo.com","addresssssss",13);
		Client client2;
		Product prod1 = new Product(1,"name","email@yahoo.com",13.55f,2103);
		Product prod2;
		Bill ord1 = new Bill(1,1,13);
		Bill ord2;
//client1 = clientBll.findclientById(12).get(0);
		try {
			System.out.println(clientBll.insert(client1));
			System.out.println(productBll.insert(prod1));
			System.out.println(orderBll.insert(ord1,prod1,client1));
			//prod1.setquantity(prod1.getQuantity()-ord1.getQuantity());
			System.out.println(orderBll.delete(ord1));
			client1.setAge(20);
			System.out.println(clientBll.update(client1));
			System.out.println(client1.getAge());
			System.out.println(clientBll.delete(client1));
			client2=clientBll.findClientById(1).get(0);
			
			System.out.println(client2);
		} catch (Exception ex) {
			LOGGER.log(Level.INFO, ex.getMessage());
		}

		// obtain field-value pairs for object through reflection
	//	ReflectionExample.retrieveProperties(client1);

	}

}