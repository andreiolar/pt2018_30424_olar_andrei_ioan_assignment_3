package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import bll.validators.EmailValidator;

import bll.validators.Validator;
import dao.ProductDAO;
import model.Client;
import model.Product;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ProductBLL {

	private List<Validator<Product>> validators;
	private ProductDAO productDAO;

	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
		
		

		productDAO = new ProductDAO();
	}
	/**
	 * Just checks if the Product with the id given exists
	 *
	 * @param  int id
	 
	 * @return     List<Product> list
	 */	

	public List<Product> findProductById(int id) {
		List<Product> list=productDAO.findById(id);
		if (list.isEmpty()) {
			
			System.out.println("The Product with id =" + id + " was not found!");
			//Product s1=new Product(0,"","","",0);
			return list;
		}
		else
		{System.out.println("The Product with id = " + id + " was found!");
			return list;
		}
	}
	/**
	 * Just checks if the Product exists, If no then it inserts it
	 *
	 * @param  Product
	 
	 * @return     String with info about insertion
	 */	
	public String insert(Product s) throws NoSuchMethodException {
		
		if (productDAO.findById(s.getId()).isEmpty()) { 
			
			return productDAO.insert(s);
			} else {  
				return ("The Product with id =" + s.getId() + " already exist!\nInsertion failed!");
			} 
		
	}
	/**
	 * Just checks if the Product exists, If yes then it deletes it
	 *
	 * @param  the Product
	 
	 * @return     String with info about deletion of the
	 */	
	public String delete(Product s) {
		if (productDAO.findById(s.getId()).isEmpty()) { 
			
			return "No such Product found!";
			} else {  
				return productDAO.delete(s);
			} 
	}
	/**
	 * Just checks if the Product exists, If yes then it updates it
	 *
	 * @param  the Product
	 
	 * @return     String with info about update of the
	 */	
	public String update(Product s) {
if (productDAO.findById(s.getId()).isEmpty()) { 
			
			return "No such Product found!";
			} else {  
				
				return productDAO.update(s);
			} 
	}
	/**
	 * 
	 *
	 * @param  the Product
	 
	 * @return     Gives a list with all products
	 */	
	public List<Product> findAll() {
		List<Product> list=productDAO.findAll();
		
		return list;
	}



	

}
