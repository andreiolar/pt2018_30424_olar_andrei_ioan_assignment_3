package bll;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import bll.validators.EmailValidator;

import bll.validators.Validator;
import dao.ClientDAO;
import dao.BillDAO;
import dao.ProductDAO;
import model.Client;
import model.Bill;
import model.Product;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class BillBLL {

	private List<Validator<Bill>> validators;
	private BillDAO billDAO;
	private ProductDAO productDAO;
	private ClientDAO clientDAO;
	public BillBLL() {
		validators = new ArrayList<Validator<Bill>>();
		
		productDAO = new ProductDAO();
		clientDAO = new ClientDAO();
		billDAO = new BillDAO();
	}
	/**
	 * Searches for the client  and if it's found then show all orders
	 *
	 * @param  a Client
	 
	 * @return    List<Bill>-> contains all orders of the client
	 */	
	public List<Bill> findOrderById(Client c) {
		List<Bill> list=billDAO.findById(c.getId());
		if (list.isEmpty()) {
			
			System.out.println("The Bill from the client with the id =" + c.getId() + " was not found!");
			//Bill s1=newOrder(0,"","","",0);
			return list;
		}
		else
		{System.out.println("The Bill from the client with id = " + c.getId() + " was found!");
			return list;
		}
	}
	/**
	 * Shows all orders
	 *
	 * @param  nothing
	 
	 * @return    List<Bill>-> contains all orders 
	 */	
	public List<Bill> findAll() {
		List<Bill> list=billDAO.findAll();
		
			return list;
		
	}
	/**
	 * Inserts a new order
	 *
	 * @param  Bill(order)
	 * @param  the product of the order
	 * @param  the client of the order
	 * @return    Returns a string with whether the insertion succeded
	 */	
	public String insert(Bill s,Product p,Client c) throws NoSuchMethodException {
if (!productDAO.findById(p.getId()).isEmpty()) { 
	if (!clientDAO.findById(c.getId()).isEmpty()) { 
		if(p.getQuantity()>=s.getQuantity()) {
				p.setQuantity(p.getQuantity()-s.getQuantity());
				String ss=billDAO.insert(s);
				try {
					recipe(c);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return ss;
			} else {  
				return ("The ordered quantity is too big!\nInsertion failed!");
			} 
	
		} else {  
			return ("The client with id =" + c.getId() + " doesn't exist!");
		} 
			
			} else {  
				return ("The Product with id =" + p.getId() + "doesn't exist!");
			} 
			
			
		
	}
	/**
	 * Creates a receipt file with the name of the client
	 * Generated automatically on insert of a new order
	 *
	 * @param  client
	 * @return    nothing
	 */	
	public void recipe(Client c) throws FileNotFoundException, UnsupportedEncodingException {
		float money=0;
		PrintWriter writer = new PrintWriter(c.getName(), "UTF-8");
		writer.println(c.getName()+" buys: ");
		for(Bill b:findOrderById(c)) {
			Product p=productDAO.findById(b.getIdprod()).get(0);
			writer.println(b.getQuantity()+" "+p.getName()+" with a total of "+p.getPrice()*b.getQuantity());
			money=p.getPrice()*b.getQuantity()+money;
		}
		
		writer.println("Everything in total: "+money);
		writer.close();
	}
	/**
	 *
	 * @param   the order that we want to delete
	 * @return    String with info about delete
	 */	
	public String delete(Bill s) {
		if (billDAO.findById(s.getId()).isEmpty()) { 
			
			return "No suchOrder found!";
			} else {  
				return billDAO.delete(s);
			} 
	}
	/**
	 *
	 * @param   the order that we want to update
	 * @return    String with info about update
	 */	
	public String update(Bill s) {
if (billDAO.findById(s.getId()).isEmpty()) { 
			
			return "No suchOrder found!";
			} else {  
				
				return billDAO.update(s);
			} 
	
	}}

