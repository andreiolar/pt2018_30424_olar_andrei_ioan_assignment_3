package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import bll.validators.EmailValidator;
import bll.validators.ClientAgeValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ClientBLL {

	private List<Validator<Client>> validators;
	private ClientDAO clientDAO;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		validators.add(new EmailValidator());
		validators.add(new ClientAgeValidator());

		clientDAO = new ClientDAO();
	}
	/**
	 * Searches for the client with the id given
	 *
	 * @param  int id
	 
	 * @return     Gives a list with the serched id
	 */	
	public List<Client> findClientById(int id) {
		List<Client> list=clientDAO.findById(id);
		if (list.isEmpty()) {
			
			System.out.println("The client with id =" + id + " was not found!");
			//client s1=new client(0,"","","",0);
			return list;
		}
		else
		{System.out.println("The client with id = " + id + " was found!");
			return list;
		}
	}
	public List<Client> findAll() {
		List<Client> list=clientDAO.findAll();
	
			return list;
	
	}
	/**
	 * Searches for the client with the id given if it's not found it inserts it
	 *
	 * @param  a Client
	 
	 * @return    String with info about insert
	 */	
	public String insert(Client s) throws NoSuchMethodException {
		
		if (clientDAO.findById(s.getId()).isEmpty()) { 
			
			return clientDAO.insert(s);
			} else {  
				return ("The client with id = " + s.getId() + " already exist!\nInsertion failed!");
			} 
		
	}
	/**
	 * Searches for the client with the id given if it's found it deletes it
	 *
	 * @param  a Client
	 
	 * @return    String with info about delete
	 */	
	public String delete(Client s) {
		if (clientDAO.findById(s.getId()).isEmpty()) { 
			
			return "No such client found!";
			} else {  
				return clientDAO.delete(s);
			} 
	}
	/**
	 * Searches for the client with the id given if it's found it updates it
	 *
	 * @param  a Client
	 
	 * @return    String with info about update
	 */	
	public String update(Client s) {
if (clientDAO.findById(s.getId()).isEmpty()) { 
			
			return "No such client found!";
			} else {  
				
				return clientDAO.update(s);
			} 
	}



	

}
