package presentation;

import model.Client;
import model.Product;
import model.Bill;
import presentation.View;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import bll.BillBLL;
import bll.ClientBLL;
import bll.ProductBLL;

public class Controller {

	
	private View view;
	ClientBLL clientBll = new ClientBLL();
	BillBLL orderBll = new BillBLL();
	ProductBLL productBll = new ProductBLL();

	public Controller(View view) {
		this.view = view;
		view.addCInsertButtonListener(new CInsertListener());
		view.addCUpdateButtonListener(new CUpdateListener());
		view.addCShowButtonListener(new CShowListener());
		view.addCDeleteButtonListener(new CDeleteListener());
		view.addCFindButtonListener(new CFindListener());
		view.addPInsertButtonListener(new PInsertListener());
		view.addPUpdateButtonListener(new PUpdateListener());
		view.addPShowButtonListener(new PShowListener());
		view.addPDeleteButtonListener(new PDeleteListener());
		view.addPFindButtonListener(new PFindListener());
		
		
		view.addBInsertButtonListener(new BInsertListener());
		view.addBDeleteButtonListener(new BDeleteListener());
		view.addBFindButtonListener(new BFindListener());
		
	}

	class CInsertListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Client client=new Client(view.getCId(),view.getCName(),view.getCAddress(),view.getCEmail(),view.getCAge());
			String s;
			try {
				s = clientBll.insert(client);
				view.setTextArea1(s);
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		}
	}
	class CUpdateListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Client client=new Client(view.getCId(),view.getCName(),view.getCAddress(),view.getCEmail(),view.getCAge());
			String s=clientBll.update(client);
			
			view.setTextArea1(s);
		}
	}
	class CShowListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//Client client=new Client(view.getCId(),view.getCName(),view.getCAddress(),view.getCEmail(),view.getCAge());
			view.makeJTable(clientBll.findAll());
		}
	}
	class CDeleteListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Client client=new Client(view.getCId(),view.getCName(),view.getCAddress(),view.getCEmail(),view.getCAge());
			String s=clientBll.delete(client);
			
			view.setTextArea1(s);
		}
	}
	class CFindListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			view.makeJTable(clientBll.findClientById(view.getCId()));
			
		
		}
	}
	class PInsertListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Product p=new Product(view.getPId(),view.getPName(),view.getPAddress(),view.getPPrice(),view.getPQuantity());
			String s;
			try {
				s = productBll.insert(p);
				view.setTextArea2(s);
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		}
	}
	class PUpdateListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Product p=new Product(view.getPId(),view.getPName(),view.getPAddress(),view.getPPrice(),view.getPQuantity());
			String s=productBll.update(p);
			
			view.setTextArea2(s);
		}
	}
	class PShowListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//Client client=new Client(view.getCId(),view.getCName(),view.getCAddress(),view.getCEmail(),view.getCAge());
			view.makeJTable(productBll.findAll());
		}
	}
	class PDeleteListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Product p=new Product(view.getPId(),view.getPName(),view.getPAddress(),view.getPPrice(),view.getPQuantity());
			String s = productBll.delete(p);
			
			view.setTextArea2(s);
		}
	}
	class PFindListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			view.makeJTable(productBll.findProductById(view.getPId()));
			
		
		}
	}
	
	
	
	
	
	
	
	class BInsertListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Product p;
			if(!productBll.findProductById(view.getBPId()).isEmpty())
			 p=productBll.findProductById(view.getBPId()).get(0);
			else {view.setTextArea3("Product not found"); return;}
			Client c;
			if(!clientBll.findClientById(view.getBCId()).isEmpty())
			c=clientBll.findClientById(view.getBCId()).get(0);
			else {view.setTextArea3("Client not found"); return;}
			
			Bill ord1 = new Bill(view.getBPId(),view.getBCId(),view.getBQuantity());
			try {
				String s=orderBll.insert(ord1,p,c);
				view.setTextArea3(s);
			} catch (NoSuchMethodException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		}
	}
	
	class BShowListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//Client client=new Client(view.getCId(),view.getCName(),view.getCAddress(),view.getCEmail(),view.getCAge());
			view.makeJTable(orderBll.findAll());
		}
	}
	class BDeleteListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Bill ord1 = new Bill(view.getBPId(),view.getBCId(),view.getBQuantity());
			
			view.setTextArea3(orderBll.delete(ord1));
		}
	}
	class BFindListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			if(!clientBll.findClientById(view.getBCId()).isEmpty()) {Client c=clientBll.findClientById(view.getBCId()).get(0);
			view.makeJTable(orderBll.findOrderById(c));
			}
			else
				view.setTextArea3("Client not found.");
		
		}
	}
	
	
	
	
	
}
