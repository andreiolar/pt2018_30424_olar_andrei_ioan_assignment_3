package presentation;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.awt.event.*;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class View extends JFrame {
	// creates stuff
	private static final String INITIAL_VALUE = "1";
	private JFrame frame1;
	private JFrame frame2;
	private JLabel clientAddressLabel;
	private JLabel clientAgeLabel;
	private JLabel clientNameLabel;
	private JLabel clientIdLabel;
	private JLabel clientEmailLabel;
	private JLabel productAddressLabel;
	private JLabel productQuantityLabel;
	private JLabel productNameLabel;
	private JLabel productIdLabel;
	private JLabel productPriceLabel;
	private JTextField clientAddressTextField;
	private JTextField clientAgeTextField;
	private JTextField clientNameTextField;
	private JTextField clientIdTextField;
	private JTextField clientEmailTextField;
	private JTextField productAddressTextField;
	private JTextField productQuantityTextField;
	private JTextField productNameTextField;
	private JTextField productIdTextField;
	private JTextField productPriceTextField;
	private JTextArea textArea1;
	private JTextArea textArea2;
	private JButton insert1Button;
	private JButton find1Button;
	private JButton delete1Button;
	private JButton show1Button;
	private JButton update1Button;

	private JButton insert2Button;
	private JButton find2Button;
	private JButton delete2Button;
	private JButton show2Button;
	private JButton update2Button;

	private JButton insert3Button;
	private JButton find3Button;
	private JButton delete3Button;
	private JTextArea textArea3;
	private JLabel BillCIdLabel;
	private JLabel BillPIdLabel;
	private JLabel BillQuantityLabel;
	private JTextField BillCIdTextField;
	private JTextField BillPIdTextField;
	private JTextField BillQuantityTextField;
	
	private JLabel maxCustomerArrivingTime;
	private JLabel minServiceTime;
	private JLabel maxServiceTime;
	private JLabel noQueues;
	private JLabel simulationTime;

	private JTextField minCustomerArrivingTimeText;
	private JTextField maxCustomerArrivingTimeText;
	private JTextField minServiceTimeText;
	private JTextField maxServiceTimeText;
	private JTextField noQueuesText;
	private JTextField simulationTimeText;
	private JFrame f;
	private JTextArea textArea;
	private JPanel content;
	private JButton startButton;

	public View() {

		JFrame frame1 = new JFrame("Client");
		JFrame frame2 = new JFrame("Product");
		JFrame frame3 = new JFrame("Bill");
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel content1 = new JPanel(new GridLayout(8, 2));
		JPanel content2 = new JPanel(new GridLayout(8, 2));
		JPanel content3 = new JPanel(new GridLayout(5, 2));
		content1.setOpaque(true);
		content2.setOpaque(true);
		content3.setOpaque(true);
		clientIdLabel = new JLabel("Id");
		clientAddressLabel = new JLabel("Address");
		clientNameLabel = new JLabel("Name");
		clientEmailLabel = new JLabel("Email");
		clientAgeLabel = new JLabel("Age");

		productIdLabel = new JLabel("Id");
		productAddressLabel = new JLabel("Address");
		productNameLabel = new JLabel("Name");
		productPriceLabel = new JLabel("Price");
		productQuantityLabel = new JLabel("Quantity");

		clientIdTextField = new JTextField(5);
		clientAddressTextField = new JTextField(5);
		clientNameTextField = new JTextField(5);
		clientEmailTextField = new JTextField(5);
		clientAgeTextField = new JTextField(5);

		productIdTextField = new JTextField(5);
		productAddressTextField = new JTextField(5);
		productNameTextField = new JTextField(5);
		productPriceTextField = new JTextField(5);
		productQuantityTextField = new JTextField(5);
		
		BillCIdLabel=new JLabel("Client Id");
		BillPIdLabel=new JLabel("Product Id");
		BillQuantityLabel=new JLabel("Quantity");
		
		BillCIdTextField=new JTextField(5);
		BillPIdTextField=new JTextField(5);
		BillQuantityTextField=new JTextField(5);
		
		insert1Button = new JButton("Insert");
		find1Button = new JButton("Find");
		delete1Button = new JButton("Delete");
		show1Button = new JButton("Show");
		insert2Button = new JButton("Insert");
		find2Button = new JButton("Find");
		delete2Button = new JButton("Delete");
		show2Button = new JButton("Show");
		update2Button = new JButton("Update");
		update1Button = new JButton("Update");
		insert3Button = new JButton("Insert");
		find3Button = new JButton("Find");
		delete3Button = new JButton("Delete");
		textArea1 = new JTextArea();
		textArea2 = new JTextArea();
		textArea3 = new JTextArea();
		content1.add(clientIdLabel, BorderLayout.CENTER);
		content1.add(clientIdTextField, BorderLayout.CENTER);

		content1.add(clientAddressLabel, BorderLayout.CENTER);
		content1.add(clientAddressTextField, BorderLayout.CENTER);

		content1.add(clientNameLabel, BorderLayout.CENTER);
		content1.add(clientNameTextField, BorderLayout.CENTER);

		content1.add(clientEmailLabel, BorderLayout.CENTER);
		content1.add(clientEmailTextField, BorderLayout.CENTER);

		content1.add(clientAgeLabel, BorderLayout.CENTER);
		content1.add(clientAgeTextField, BorderLayout.CENTER);

		content1.add(insert1Button, BorderLayout.CENTER);
		content1.add(find1Button, BorderLayout.CENTER);
		content1.add(delete1Button, BorderLayout.CENTER);
		content1.add(show1Button, BorderLayout.CENTER);
		content1.add(update1Button, BorderLayout.CENTER);
		content1.add(textArea1);

		content2.add(productIdLabel, BorderLayout.CENTER);
		content2.add(productIdTextField, BorderLayout.CENTER);

		content2.add(productAddressLabel, BorderLayout.CENTER);
		content2.add(productAddressTextField, BorderLayout.CENTER);

		content2.add(productNameLabel, BorderLayout.CENTER);
		content2.add(productNameTextField, BorderLayout.CENTER);

		content2.add(productPriceLabel, BorderLayout.CENTER);
		content2.add(productPriceTextField, BorderLayout.CENTER);

		content2.add(productQuantityLabel, BorderLayout.CENTER);
		content2.add(productQuantityTextField, BorderLayout.CENTER);

		content2.add(insert2Button, BorderLayout.CENTER);
		content2.add(find2Button, BorderLayout.CENTER);
		content2.add(delete2Button, BorderLayout.CENTER);
		content2.add(show2Button, BorderLayout.CENTER);
		content2.add(update2Button, BorderLayout.CENTER);
		content2.add(textArea2);
		
		content3.add(BillCIdLabel);
		content3.add(BillCIdTextField);
		
		content3.add(BillPIdLabel);
		content3.add(BillPIdTextField);
		
		content3.add(BillQuantityLabel);
		content3.add(BillQuantityTextField);
		content3.add(insert3Button);
		content3.add(find3Button);
		content3.add(delete3Button);
		content3.add(textArea3);
		
		
		frame1.pack();
		frame2.pack();
		frame3.pack();
		frame1.setVisible(true);
		frame2.setVisible(true);
		frame3.setVisible(true);
		content = new JPanel(new CardLayout());
		content.setOpaque(true); // content panes must be opaque
		content1.setOpaque(true); // content panes must be opaque
		content2.setOpaque(true); // content panes must be opaque
		content3.setOpaque(true); // content panes must be opaque
		frame1.setContentPane(content1);
		frame2.setContentPane(content2);
		frame3.setContentPane(content3);
		frame1.setSize(200, 200);
		frame2.setSize(200, 200);
		frame3.setSize(200, 200);
		

	}

	public void addCShowButtonListener(ActionListener mal) {
		show1Button.addActionListener(mal);

	}

	public void addCInsertButtonListener(ActionListener mal) {
		insert1Button.addActionListener(mal);

	}

	public void addCUpdateButtonListener(ActionListener mal) {
		update1Button.addActionListener(mal);

	}

	public void addCDeleteButtonListener(ActionListener mal) {
		delete1Button.addActionListener(mal);

	}

	public void addCFindButtonListener(ActionListener mal) {
		find1Button.addActionListener(mal);

	}

	public void addPShowButtonListener(ActionListener mal) {
		show2Button.addActionListener(mal);

	}

	public void addPInsertButtonListener(ActionListener mal) {
		insert2Button.addActionListener(mal);

	}

	public void addPUpdateButtonListener(ActionListener mal) {
		update2Button.addActionListener(mal);

	}

	public void addPDeleteButtonListener(ActionListener mal) {
		delete2Button.addActionListener(mal);

	}

	public void addPFindButtonListener(ActionListener mal) {
		find2Button.addActionListener(mal);

	}

	
	public void addBInsertButtonListener(ActionListener mal) {
		insert3Button.addActionListener(mal);

	}

	public void addBDeleteButtonListener(ActionListener mal) {
		delete3Button.addActionListener(mal);

	}

	public void addBFindButtonListener(ActionListener mal) {
		find3Button.addActionListener(mal);

	}
	public int getBCId() {

		return Integer.parseInt(BillCIdTextField.getText());
	}
	
	public int getBPId() {

		return Integer.parseInt(BillPIdTextField.getText());
	}
	public int getBQuantity() {

		return Integer.parseInt(BillQuantityTextField.getText());
	}
	public int getCId() {

		return Integer.parseInt(clientIdTextField.getText());
	}

	public String getCAddress() {

		return clientAddressTextField.getText();
	}

	public int getCAge() {

		return Integer.parseInt(clientAgeTextField.getText());
	}

	public String getCEmail() {

		return clientEmailTextField.getText();
	}

	public String getCName() {

		return clientNameTextField.getText();
	}

	public int getPId() {

		return Integer.parseInt(productIdTextField.getText());
	}

	public String getPAddress() {

		return productAddressTextField.getText();
	}

	public int getPQuantity() {

		return Integer.parseInt(productQuantityTextField.getText());
	}

	public Float getPPrice() {

		return Float.parseFloat(productPriceTextField.getText());
	}

	public String getPName() {

		return productNameTextField.getText();
	}
	/**
	 * Makes a window with a jTable containing the information given from the input list
	 *
	 * @param  List<T> list
	 
	 * @return      Nothing
	 */	
	public <T> void makeJTable(List<T> list) {
		JFrame frame = new JFrame();
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		List<Object> columns = new ArrayList<Object>();
		List<Object> val = new ArrayList<Object>();
		List<Object[]> values = new ArrayList<Object[]>();
		Object[] colos = new Object[] { "frame1", "frame2", "frame3" };
		Object[][] colo = new Object[][] { { "frame11", 22, "frame33", "pls", "munca" },
				{ "frame11", 44, "frame33", "pls", "munca" }, { "frame11", 55, "frame33", "pls", "munca" } };
		
		try {for (Object s : list) {
			for (Field field : s.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				System.out.println(field.getName());
				System.out.println("Intra");
				columns.add(field.getName());
			}
			break;
		}
			for (Object s : list) {

				for (Field field : s.getClass().getDeclaredFields()) {
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), s.getClass());
					Method method = propertyDescriptor.getReadMethod();
					Object value = method.invoke(s);
					val.add(value);
				}
				values.add(val.toArray());
				val.clear();

			}

			System.out.println(columns);
			System.out.println(values);
			for (Object pls : columns.toArray())
				System.out.println(pls);
			for (Object pls : values.toArray())
				System.out.println(pls);
			Object[][] array = values.toArray(new Object[values.size()][]);
			TableModel tableModel = new DefaultTableModel(colo, colos);
			JTable table = new JTable(tableModel);
			JScrollPane scroll = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
					JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			table.setPreferredScrollableViewportSize(new Dimension(420, 250));
			table.setFillsViewportHeight(true);
			/*
			 * testJFrame.add(scroll); testJFrame.setLayout(new BorderLayout());
			 * testJFrame.add(new JScrollPane(table));
			 * testJFrame.add(table.getTableHeader());
			 * frame.add(testJFrame,BorderLayout.NORTH); frame.pack();
			 * frame.setVisible(true); frame.setSize(200, 200);
			 */

			JTable table1 = new JTable(array, columns.toArray());

			JScrollPane scrollPane = new JScrollPane(table1);
			frame.add(scrollPane, BorderLayout.CENTER);
			frame.setSize(300, 150);
			frame.setVisible(true);

		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IntrospectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		}
	}

	public void setTextArea(String s) {
		// content.removeAll();
		textArea.setText(s);
	}

	public void setTextArea1(String s) {
		// TODO Auto-generated method stub
		textArea1.setText(s);

	}

	public void setTextArea2(String s) {
		// TODO Auto-generated method stub
		textArea2.setText(s);

	}

	public void setTextArea3(String s) {
		// TODO Auto-generated method stub
		textArea3.setText(s);
		
	}

}
