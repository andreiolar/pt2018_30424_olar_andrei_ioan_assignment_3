package dao;

import java.util.List;

import model.Client;

public class ClientDAO extends AbstractDAO<Client> {


public ClientDAO(){
	super();
}

public  String insert(Client s ) throws NoSuchMethodException  {
	return super.insert(s);
	
}
public String delete(Client s) {
	return super.delete(s);
}
public String update(Client s) {
	return super.update(s);
}
public List<Client> findById(int id) {
	return super.findById(id);
}
public List<Client> findAll() {
	return super.findAll();
}
	// uses basic CRUD methods from superclass

	// TODO: create only student specific queries
}
