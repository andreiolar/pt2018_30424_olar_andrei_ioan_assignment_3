package dao;

import java.util.List;

import model.Bill;

public class BillDAO extends AbstractDAO<Bill> {


public BillDAO(){
	super();
}

public  String insert(Bill s ) throws NoSuchMethodException  {
	return super.insert(s);
	
}
public String delete(Bill s) {
	return super.delete(s);
}
public String update(Bill s) {
	return super.update(s);
}
public List<Bill> findById(int id) {
	return super.findById(id);
}
public List<Bill> findAll() {
	return super.findAll();
}
	// uses basic CRUD methods from superclass

	// TODO: create only student specific queries
}