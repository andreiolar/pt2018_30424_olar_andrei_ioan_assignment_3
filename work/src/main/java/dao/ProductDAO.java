package dao;

import java.util.List;

import model.Product;

public class ProductDAO extends AbstractDAO<Product> {


public ProductDAO(){
	super();
}

public  String insert(Product s ) throws NoSuchMethodException  {
	return super.insert(s);
	
}
public String delete(Product s) {
	return super.delete(s);
}
public String update(Product s) {
	return super.update(s);
}
public List<Product> findById(int id) {
	return super.findById(id);
}
public List<Product> findAll() {
	return super.findAll();
}
	// uses basic CRUD methods from superclass

	// TODO: create only student specific queries
}