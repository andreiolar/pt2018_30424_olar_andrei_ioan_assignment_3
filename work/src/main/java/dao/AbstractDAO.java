package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 * @Source http://www.java-blog.com/mapping-javaobjects-database-reflection-generics
 */
public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

	}
	/**
	 * Returns a query as string
	 *
	 * @param  field name as String
	 * 
	 * @return      the select query as string
	 */
	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}
	/**
	 * Returns a query as string
	 *
	 * @param  field name as String
	 * 
	 * @return      the delete query as string
	 */	
private String createDeleteQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}
/**
 * Returns a query as string
 *
 * @param  Field[] (array of fields)
 
 * @return      the insert query as string
 */
	private String createInsertQuery(Field[] fields) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName());
		sb.append(" (");
		int i=0;
		for (Field field:fields)
			{i++;
			if(i==1)
			sb.append("  "+field.getName()+" ");
			else
				sb.append(" , "+field.getName()+" ");}
		sb.append(" ) VALUES ( ? ");
		for (int j=i;j>1;j--)
			sb.append(" , ? ");
		sb.append(" ) ");

		return sb.toString();
	}
	/**
	 * Returns a query as string
	 *
	 * @param  Field[] (array of fields)
	 
	 * @return      the update query as string
	 */
	private String createUpdateQuery(Field[] fields) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName());
		sb.append(" SET ");
		int i=0;
		for (Field field:fields)
		{if(field.getName()!="id") {
			i++;
		if(i==1)
		sb.append("  "+field.getName()+" = ?");
		else
			sb.append(" , "+field.getName()+" = ?");}}
	
		sb.append(" WHERE id=? ");

		return sb.toString();
	}

	/**
	 * Returns a query as string
	 *
	 * @param  nothing
	 
	 * @return      the Select ALL query as string
	 */	
private String createSelectAllQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(" * ");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		return sb.toString();
	}
/**
 * Returns a List<T> with all values from the table
 *
 * @param  nothing
 
 * @return      List<T>
 */	

	public List<T> findAll() {
		// TODO:
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectAllQuery();
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			// statement.setInt(1, id);
			resultSet = statement.executeQuery();

			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return new ArrayList<T>();
	}
	/**
	 * Returns a List<T> with all values from the table with id = input
	 *
	 * @param  int id
	 
	 * @return      List<T>
	 */	

	public List<T> findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		System.out.println("We here yet");
		return null;
	}

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<T>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}
	/**
	 * Returns a String with information about the insert
	 *
	 * @param  T t
	 
	 * @return      String s with degree of success
	 */	
	@SuppressWarnings("unlikely-arg-type")
	public String insert(T t) throws NoSuchMethodException {
		// TODO:
		Connection connection = null;
		PreparedStatement statement = null;

		try {
			String query = createInsertQuery( type.getDeclaredFields());
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			
			int i = 0;
			for (Field field : type.getDeclaredFields()) {
				field.setAccessible(true);
				i++;
				Object value = field.get(t);
				
					statement.setObject (i , value);		
			}
			statement.executeUpdate();
			return "This " + t.getClass() .getName()+ " was inserted!";
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		return "This " + t.getClass() .getName()+ " was not inserted!";
	}
	/**
	 * Returns a String with information about the update
	 *
	 * @param  T t
	 
	 * @return      String s with degree of success
	 */	
	public String update(T t) {
		// TODO:
		Connection connection = null;
		PreparedStatement statement = null;
		
		String query = createUpdateQuery(t.getClass().getDeclaredFields());
		
		int i=0;
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			
			for (Field field : (t.getClass().getDeclaredFields())) {
				field.setAccessible(true);
				if(field.getName()!="id") {
				i++;
				Object value = field.get(t);
				statement.setObject (i , value);	
				}
				else{
					statement.setInt(t.getClass().getDeclaredFields().length,(Integer) field.get(t));
				}
			}
			statement.executeUpdate();
			return "Update successful!";
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return "Update error!";
		
	}

	/**
	 * Returns a String with information about the delete
	 *
	 * @param  T t
	 
	 * @return      String s with degree of success
	 */	
	public String delete(T t) {

		Connection connection = null;
		PreparedStatement statement = null;
		String query = createDeleteQuery("id");
		try {	
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			Field field=(t.getClass()).getDeclaredField("id");
			field.setAccessible(true);
			statement.setInt(1, (Integer) (field.get(t)));
			statement.executeUpdate();
			return "Deletion successfull!";
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
		} catch (IllegalArgumentException e) {LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		
		return "Deletion failed!";
	}
}
