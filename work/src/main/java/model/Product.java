package model;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Product {
	private int id;
	private String name;
	private String address;
	private float price;
	private int quantity;

	public Product() {
	}

	public Product(int id, String name, String address,float price, int quantity) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.price = price;
		this.quantity = quantity;
	}

	public Product(String name, String address, float price, int quantity) {
		super();
		this.name = name;
		this.address = address;
		this.price = price;
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", address=" + address + ", supplier=" + price + ", quantity=" + quantity
				+ "]";
	}

}
