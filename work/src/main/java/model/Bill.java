package model;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class Bill {
	private int id;
	private int idprod;

	private int quantity;

	public Bill() {
	}

	public Bill(int id, int idprod, int quantity) {
		super();
		this.id = id;
		this.idprod = idprod;

		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdprod() {
		return idprod;
	}

	public void setIdprod(int idprod) {
		this.idprod = idprod;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Bill [id=" + id + ", idprod=" + idprod + ", quantity=" + quantity + "]";
	}

}
